# PEW PEW laser notes

![photo of PEW PEW laser notes booklet](https://raw.githubusercontent.com/SuperDoxin/Pew-pew-laser-notes/master/laser/pictures/front.png "photo of PEW PEW laser notes booklet")

a notebook to write down lasercutter power and speed settings.

## Assembly

everything is made to work with a4 paper size, resulting in a7 booklets.

print one [cover set](https://github.com/SuperDoxin/Pew-pew-laser-notes/raw/master/laser/renders/coverset.pdf) on firm colored paper.

print as many [page sets](https://github.com/SuperDoxin/Pew-pew-laser-notes/blob/master/laser/renders/pageset.pdf) as you think you need.
print double sided. if your printer can't print duplex you'll have to feed the paper in a second time, making sure it prints on the backside with the text the same side up as the front. holding it up to a light should show horizontally mirrored text.

cut all your sheets into quarters. fold the cover in half to find the centerline and place it atop your stack of pages unfolded. staple along the centerline.

fold your booklet in half, and if desired trim the edges.
